#ifndef CARDSWINDOW_H
#define CARDSWINDOW_H

#include <QMainWindow>

namespace Ui {
class CardsWindow;
}

class CardsWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit CardsWindow(QWidget *parent = 0);
    ~CardsWindow();

private:
    Ui::CardsWindow *ui;
};

#endif // CARDSWINDOW_H
