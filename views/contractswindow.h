#ifndef CONTRACTSWINDOW_H
#define CONTRACTSWINDOW_H

#include <QMainWindow>

namespace Ui {
class ContractsWindow;
}

class ContractsWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ContractsWindow(QWidget *parent = 0);
    ~ContractsWindow();

private:
    Ui::ContractsWindow *ui;
};

#endif // CONTRACTSWINDOW_H
