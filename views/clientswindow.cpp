#include "clientswindow.h"
#include "ui_clientswindow.h"

ClientsWindow::ClientsWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ClientsWindow)
{
    ui->setupUi(this);
}

ClientsWindow::~ClientsWindow()
{
    delete ui;
}
