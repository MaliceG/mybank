#ifndef ACCOUNTSWINDOW_H
#define ACCOUNTSWINDOW_H

#include <QMainWindow>

namespace Ui {
class AccountsWindow;
}

class AccountsWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit AccountsWindow(QWidget *parent = 0);
    ~AccountsWindow();

private:
    Ui::AccountsWindow *ui;
};

#endif // ACCOUNTSWINDOW_H
