#include "paymentswindow.h"
#include "ui_paymentswindow.h"

PaymentsWindow::PaymentsWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::PaymentsWindow)
{
    ui->setupUi(this);
}

PaymentsWindow::~PaymentsWindow()
{
    delete ui;
}
