#ifndef CLIENTSWINDOW_H
#define CLIENTSWINDOW_H

#include <QMainWindow>

namespace Ui {
class ClientsWindow;
}

class ClientsWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ClientsWindow(QWidget *parent = 0);
    ~ClientsWindow();

private:
    Ui::ClientsWindow *ui;
};

#endif // CLIENTSWINDOW_H
