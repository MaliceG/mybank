#include "cardswindow.h"
#include "ui_cardswindow.h"

CardsWindow::CardsWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CardsWindow)
{
    ui->setupUi(this);
}

CardsWindow::~CardsWindow()
{
    delete ui;
}
