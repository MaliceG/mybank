#ifndef PAYMENTSWINDOW_H
#define PAYMENTSWINDOW_H

#include <QMainWindow>

namespace Ui {
class PaymentsWindow;
}

class PaymentsWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit PaymentsWindow(QWidget *parent = 0);
    ~PaymentsWindow();

private:
    Ui::PaymentsWindow *ui;
};

#endif // PAYMENTSWINDOW_H
