#include "contractswindow.h"
#include "ui_contractswindow.h"

ContractsWindow::ContractsWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ContractsWindow)
{
    ui->setupUi(this);
}

ContractsWindow::~ContractsWindow()
{
    delete ui;
}
