#-------------------------------------------------
#
# Project created by QtCreator 2020-01-18T23:26:06
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MyBank
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    authwindow.cpp \
    views/paymentswindow.cpp \
    views/cardswindow.cpp \
    views/accountswindow.cpp \
    views/clientswindow.cpp \
    views/contractswindow.cpp

HEADERS += \
        mainwindow.h \
    authwindow.h \
    views/paymentswindow.h \
    views/cardswindow.h \
    views/accountswindow.h \
    views/clientswindow.h \
    views/contractswindow.h

FORMS += \
        mainwindow.ui \
    authwindow.ui \
    views/paymentswindow.ui \
    views/cardswindow.ui \
    views/accountswindow.ui \
    views/clientswindow.ui \
    views/contractswindow.ui
