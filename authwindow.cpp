#include "authwindow.h"
#include "ui_authwindow.h"

#include <QMessageBox>

AuthWindow::AuthWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::AuthWindow)
{
    ui->setupUi(this);

    connect(ui->pbOk, SIGNAL(clicked(bool)), this, SLOT(connectToDatabase()));
    connect(ui->leLogin, SIGNAL(returnPressed()), this, SLOT(connectToDatabase()));
    connect(ui->lePassword, SIGNAL(returnPressed()), this, SLOT(connectToDatabase()));
    connect(ui->pbCancel, &QPushButton::clicked, this, &AuthWindow::close);

    dataBase = QSqlDatabase::addDatabase("QPSQL");
}

AuthWindow::~AuthWindow()
{
    delete ui;
}

void AuthWindow::connectToDatabase()
{
    dataBase.setHostName("localhost");
    dataBase.setPort(5432);
    dataBase.setDatabaseName("MyBank");
    dataBase.setUserName(ui->leLogin->text());
    dataBase.setPassword(ui->lePassword->text());
    bool isOpen = dataBase.open();

    if (!isOpen)
    {
        QMessageBox::warning(this, "Ошибка", "Не удалось авторизоваться!");
        return;
    }

    MainWindow *mainWindow = new MainWindow(this);
    mainWindow->show();
}

void AuthWindow::exit()
{
    this->close();
}
