#ifndef AUTHWINDOW_H
#define AUTHWINDOW_H

#include <QMainWindow>
#include "mainwindow.h"

#include <QtSql>

namespace Ui {
class AuthWindow;
}

class AuthWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit AuthWindow(QWidget *parent = 0);
    ~AuthWindow();

private:
    Ui::AuthWindow *ui;
    QSqlDatabase dataBase;

private slots:
    void connectToDatabase();
    void exit();

};

#endif // AUTHWINDOW_H
